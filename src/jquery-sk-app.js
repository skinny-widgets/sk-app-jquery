
import { SkAppImpl }  from '../../sk-app/src/impl/sk-app-impl.js';

export class JquerySkApp extends SkAppImpl {

    get prefix() {
        return 'jquery';
    }

    get suffix() {
        return 'app';
    }

}
